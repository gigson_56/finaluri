package com.example.finalurigamocda.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.finalurigamocda.R
import com.google.firebase.auth.FirebaseAuth


class SettingsFragment : Fragment(R.layout.fragment_settings) {

        private lateinit var editTextPassword: EditText
        private lateinit var buttonPasswordChange: Button

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
        ): View? {
            return inflater.inflate(R.layout.fragment_settings, container, false)
        }

            override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
                super.onViewCreated(view, savedInstanceState)
                init(view)
                regiserListeners(view)


    }
            private fun init(view: View){
                editTextPassword = view.findViewById(R.id.editTextPassword)
                buttonPasswordChange = view.findViewById(R.id.buttonPasswordChange)
            }

        private fun regiserListeners(view: View){
            buttonPasswordChange.setOnClickListener {
                val newPassword = editTextPassword.text.toString()
                if(newPassword.isEmpty()){
                    Toast.makeText(editTextPassword.context, "Empty!", Toast.LENGTH_SHORT).show()
                }
                FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)?.addOnCompleteListener {
                    task ->
                    if(task.isSuccessful){
                        Toast.makeText(editTextPassword.context, "Successful!", Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(editTextPassword.context, "Error occurred while changing password!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }
