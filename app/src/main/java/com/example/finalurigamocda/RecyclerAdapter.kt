package com.example.finalurigamocda

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var titles = arrayOf("BMW M5", "Merc E270", "Golf 3", "Honda Fit", "Maserati MC20", "Carrera", "C300", "BMW E46", "Subaru", "Jeep", "Opel", "Camry", "Prius")
    private var price = arrayOf("U can't rent a legends car..", "Price: 100$", "Price: 40$", "Price: 70$", "Price: 1350$", "Price: 800$", "Price: 450$", "Price: 150$", "Price: 120$", "Price: 500$", "price 70$", "price 90$", "ეს წაიყვანეთ და ჩვენ გადაგიხდით")
    private var images = intArrayOf(R.drawable.m5ee34, R.drawable.cdi270, R.drawable.golf3, R.drawable.hon3dafit, R.drawable.maseratimc20, R.drawable.porsche992, R.drawable.c300, R.drawable.e46, R.drawable.stukbaru, R.drawable.jipi3, R.drawable.op3l, R.drawable.toy0tacamry, R.drawable.priusii )




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return ViewHolder(v)

    }

    override fun onBindViewHolder(holder: RecyclerAdapter.ViewHolder, position: Int) {
        holder.itemTitle.text = titles[position]
        holder.itemPrice.text = price[position]
        holder.itemImage.setImageResource(images[position])
        holder.itemRent.setOnClickListener {
            Toast.makeText(holder.itemRent.context, "We will contact you on your E-mail", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var itemImage: ImageView
        var itemTitle: TextView
        var itemPrice: TextView
        var itemRent: Button

        init {
            itemImage = itemView.findViewById(R.id.item_image)
            itemTitle = itemView.findViewById(R.id.item_title)
            itemPrice = itemView.findViewById(R.id.item_price)
            itemRent = itemView.findViewById(R.id.rentBtn)
        }

    }


}