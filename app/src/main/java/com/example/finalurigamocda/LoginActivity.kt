package com.example.finalurigamocda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
class LoginActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var noAccountTv: TextView
    private lateinit var passwordresetTv: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
        loginlistener()
    }
    private fun init(){
        editTextEmail = findViewById(R.id.emailEt)
        editTextPassword = findViewById(R.id.passwordEt)
        buttonLogin = findViewById(R.id.loginBtn)
        noAccountTv = findViewById(R.id.noAccountTv)
        passwordresetTv = findViewById(R.id.passwordresetTv)
    }


    private fun loginlistener() {
        noAccountTv.setOnClickListener {
            val intent = Intent(this@LoginActivity, SignUpActivity::class.java)
            startActivity(intent)
        }
        passwordresetTv.setOnClickListener {
            val intent2 = Intent(this@LoginActivity, PasswordResetActivity::class.java)
            startActivity((intent2))
        }
        buttonLogin.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if(email.isEmpty()||password.isEmpty()){
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        gotoProfile()
                    }
                    else {
                        Toast.makeText(this, "User is not registered", Toast.LENGTH_SHORT).show()

                    }
                }
        }
    }
    private fun gotoProfile(){
        startActivity(Intent(this@LoginActivity, AppActivity::class.java))
        finish()
    }
}