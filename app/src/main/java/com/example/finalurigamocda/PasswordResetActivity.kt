package com.example.finalurigamocda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class PasswordResetActivity : AppCompatActivity() {
    private lateinit var passwordResetBtn : Button
    private lateinit var editTextEmail : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_reset)
        init()
        registerListeners()
    }
    private fun init(){
        passwordResetBtn = findViewById(R.id.passwordResetBtn)
        editTextEmail = findViewById(R.id.emailEt)
        }
    private fun registerListeners(){
        passwordResetBtn.setOnClickListener {
            val email = editTextEmail.text.toString()
            if(email.isEmpty()){
                Toast.makeText(this, "Please input E-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        Toast.makeText(this, "Check your E-mail for password reset instructions", Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this, "This user is not registered", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }

}